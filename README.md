# Vagrant & Ansible
README File for Vagrant-Ansible Repo

Date: 16-Apr-2020<br>
Maintainer: Thao Tran (<thao.tran@ez.com>)

---
<br>
<br>

## Prerequisites:
  
### VirtualBox 6.1.6

### Vagrant 2.2.7
```
ansmgmt                   running (virtualbox)
weblb                     running (virtualbox)
websrv01                  running (virtualbox)
websrv02                  running (virtualbox)
websrv03                  running (virtualbox)
websrv04                  running (virtualbox)
```

### Ubuntu 16.04.6 LTS
```
NAME="Ubuntu"
VERSION="16.04.6 LTS (Xenial Xerus)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 16.04.6 LTS"
VERSION_ID="16.04"
HOME_URL="http://www.ubuntu.com/"
SUPPORT_URL="http://help.ubuntu.com/"
BUG_REPORT_URL="http://bugs.launchpad.net/ubuntu/"
VERSION_CODENAME=xenial
UBUNTU_CODENAME=xenial
```
  
### Ansible 2.9.7
```
ansible 2.9.7
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/thao.tran/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.12 (default, Apr 15 2020, 17:07:12) [GCC 5.4.0 20160609]
```

## Quick Start:
- Ansible managemnt node is named `ansmgmt`
- Web frontend node is named `weblb`, installed HAProxy
- There are 4 backend web nodes: `websrv{01..04}`

## Example:
<b>URL for webpage:</b> http://localhost:8080
<br>
<b>HAProxy Stats:</b> http://localhost:30300/haproxy?stats with account access (`haproxy`/`haproxy`)

### Curl Example
```html
thao.tran@DESKTOP-ONAF024 ~
$ while true; do curl -is http://localhost:8080 ; sleep 3 ; done
HTTP/1.1 200 OK
Date: Sun, 26 Apr 2020 16:31:19 GMT
Content-Type: text/html
Content-Length: 624
X-Backend-Server: websrv04
Cache-Control: private
Accept-Ranges: bytes

<html>
<title>Tiny Demo</title>

<style>.block {text-align: center;margin-bottom:10px;}.block:before {content: '';display: inline-block;height: 100%;vertical-align: middle;margin-right: -0.25em;}.centered {display: inline-block;vertical-align: middle;width: 300px;}</style>

<body>
<div class="block" style="height: 99%;">
    <div class="centered">
        <h1>Tiny Demo</h1>
        <p>Served by <b>websrv04</b> (10.0.15.54).</p><br>
        <p>Maintainer:: Thao Tran <thao.tran@ez.com></p>
        <p>Copyright 2020, Ez, LLC.</p>
        <p>All rights reserved - Do Not Redistribute</p>
    </div>
</div>
</body>
</html>
HTTP/1.1 200 OK
Date: Sun, 26 Apr 2020 16:31:22 GMT
Content-Type: text/html
Content-Length: 624
X-Backend-Server: websrv01
Cache-Control: private
Accept-Ranges: bytes

<html>
<title>Tiny Demo</title>

<style>.block {text-align: center;margin-bottom:10px;}.block:before {content: '';display: inline-block;height: 100%;vertical-align: middle;margin-right: -0.25em;}.centered {display: inline-block;vertical-align: middle;width: 300px;}</style>

<body>
<div class="block" style="height: 99%;">
    <div class="centered">
        <h1>Tiny Demo</h1>
        <p>Served by <b>websrv01</b> (10.0.15.51).</p><br>
        <p>Maintainer:: Thao Tran <thao.tran@ez.com></p>
        <p>Copyright 2020, Ez, LLC.</p>
        <p>All rights reserved - Do Not Redistribute</p>
    </div>
</div>
</body>
</html>
HTTP/1.1 200 OK
Date: Sun, 26 Apr 2020 16:31:26 GMT
Content-Type: text/html
Content-Length: 624
X-Backend-Server: websrv02
Cache-Control: private
Accept-Ranges: bytes

<html>
<title>Tiny Demo</title>

<style>.block {text-align: center;margin-bottom:10px;}.block:before {content: '';display: inline-block;height: 100%;vertical-align: middle;margin-right: -0.25em;}.centered {display: inline-block;vertical-align: middle;width: 300px;}</style>

<body>
<div class="block" style="height: 99%;">
    <div class="centered">
        <h1>Tiny Demo</h1>
        <p>Served by <b>websrv02</b> (10.0.15.52).</p><br>
        <p>Maintainer:: Thao Tran <thao.tran@ez.com></p>
        <p>Copyright 2020, Ez, LLC.</p>
        <p>All rights reserved - Do Not Redistribute</p>
    </div>
</div>
</body>
</html>
HTTP/1.1 200 OK
Date: Sun, 26 Apr 2020 16:31:29 GMT
Content-Type: text/html
Content-Length: 624
X-Backend-Server: websrv03
Cache-Control: private
Accept-Ranges: bytes

<html>
<title>Tiny Demo</title>

<style>.block {text-align: center;margin-bottom:10px;}.block:before {content: '';display: inline-block;height: 100%;vertical-align: middle;margin-right: -0.25em;}.centered {display: inline-block;vertical-align: middle;width: 300px;}</style>

<body>
<div class="block" style="height: 99%;">
    <div class="centered">
        <h1>Tiny Demo</h1>
        <p>Served by <b>websrv03</b> (10.0.15.53).</p><br>
        <p>Maintainer:: Thao Tran <thao.tran@ez.com></p>
        <p>Copyright 2020, Ez, LLC.</p>
        <p>All rights reserved - Do Not Redistribute</p>
    </div>
</div>
</body>
</html>
HTTP/1.1 200 OK
Date: Sun, 26 Apr 2020 16:31:32 GMT
Content-Type: text/html
Content-Length: 624
X-Backend-Server: websrv04
Cache-Control: private
Accept-Ranges: bytes

<html>
<title>Tiny Demo</title>

<style>.block {text-align: center;margin-bottom:10px;}.block:before {content: '';display: inline-block;height: 100%;vertical-align: middle;margin-right: -0.25em;}.centered {display: inline-block;vertical-align: middle;width: 300px;}</style>

<body>
<div class="block" style="height: 99%;">
    <div class="centered">
        <h1>Tiny Demo</h1>
        <p>Served by <b>websrv04</b> (10.0.15.54).</p><br>
        <p>Maintainer:: Thao Tran <thao.tran@ez.com></p>
        <p>Copyright 2020, Ez, LLC.</p>
        <p>All rights reserved - Do Not Redistribute</p>
    </div>
</div>
</body>
</html>


thao.tran@DESKTOP-ONAF024 ~
```

### HAProxy Logs
```javascript
Apr 26 23:37:36 weblbsrv haproxy[25474]: 10.0.2.2:56119 [26/Apr/2020:23:37:36.497] weblb webapp/websrv01 0/0/0/0/0 200 839 - - ---- 3/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:37:39 weblbsrv haproxy[25474]: 10.0.2.2:56121 [26/Apr/2020:23:37:39.838] weblb webapp/websrv02 9/0/0/1/10 200 839 - - ---- 3/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:37:43 weblbsrv haproxy[25474]: 10.0.2.2:56123 [26/Apr/2020:23:37:43.199] weblb webapp/websrv03 0/0/0/1/1 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:37:46 weblbsrv haproxy[25474]: 10.0.2.2:56125 [26/Apr/2020:23:37:46.528] weblb webapp/websrv04 0/0/0/1/1 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"

Apr 26 23:37:49 weblbsrv haproxy[25474]: 10.0.2.2:56127 [26/Apr/2020:23:37:49.884] weblb webapp/websrv01 10/0/0/1/11 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:37:53 weblbsrv haproxy[25474]: 10.0.2.2:56129 [26/Apr/2020:23:37:53.229] weblb webapp/websrv02 0/0/0/0/0 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:37:56 weblbsrv haproxy[25474]: 10.0.2.2:56131 [26/Apr/2020:23:37:56.605] weblb webapp/websrv03 9/0/1/1/11 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"
Apr 26 23:38:00 weblbsrv haproxy[25474]: 10.0.2.2:56133 [26/Apr/2020:23:38:00.002] weblb webapp/websrv04 0/0/0/0/0 200 839 - - ---- 1/1/0/1/0 0/0 "GET / HTTP/1.1"
```

```javascript
Apr 26 23:36:38 weblbsrv haproxy[25474]: 10.0.2.2:56106 [26/Apr/2020:23:36:38.992] stats stats/<STATS> 0/0/0/0/0 200 22751 - - LR-- 2/2/0/0/0 0/0 "GET /haproxy?stats HTTP/1.1"
Apr 26 23:36:39 weblbsrv haproxy[25474]: 10.0.2.2:56106 [26/Apr/2020:23:36:38.992] stats stats/<NOSRV> 684/-1/-1/-1/684 503 212 - - SC-- 2/2/0/0/0 0/0 "GET /favicon.ico HTTP/1.1"
Apr 26 23:36:44 weblbsrv haproxy[25474]: 10.0.2.2:56107 [26/Apr/2020:23:36:38.992] stats stats/<STATS> 5684/0/0/0/5684 200 22754 - - LR-- 2/2/0/0/0 0/0 "GET /haproxy?stats HTTP/1.1"
Apr 26 23:36:45 weblbsrv haproxy[25474]: 10.0.2.2:56107 [26/Apr/2020:23:36:44.675] stats stats/<NOSRV> 1014/-1/-1/-1/1014 503 212 - - SC-- 1/1/0/0/0 0/0 "GET /favicon.ico HTTP/1.1"
Apr 26 23:36:50 weblbsrv haproxy[25474]: 10.0.2.2:56113 [26/Apr/2020:23:36:50.990] stats stats/<STATS> 1/0/0/0/1 200 22754 - - LR-- 2/2/0/0/0 0/0 "GET /haproxy?stats HTTP/1.1"
Apr 26 23:36:51 weblbsrv haproxy[25474]: 10.0.2.2:56113 [26/Apr/2020:23:36:50.992] stats stats/<NOSRV> 678/-1/-1/-1/678 503 212 - - SC-- 2/2/0/0/0 0/0 "GET /favicon.ico HTTP/1.1"
```